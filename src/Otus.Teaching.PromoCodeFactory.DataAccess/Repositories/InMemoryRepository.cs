﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List <T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<Guid> AddAsync(T entity)
        {
            var id = Guid.NewGuid();
            entity.Id = id;
            Data.Add(entity);
            return await Task.FromResult(id);
        }

        public async Task<bool> RemoveAsync(Guid id)
        {
            var data = await GetByIdAsync(id);
            if (data != null)
            { 
              Data.Remove(data);
                return true;
            }
            return false;
        }

        public async Task<bool> EditAsync(T data)
        { 
           var editData = await GetByIdAsync(data.Id);
            if (editData != null)
            {
                Data[Data.IndexOf(editData)] = data;
                return true;
            }

            return false;
        }


              
    }
}