﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddAsync(AddEmployeeRequest employeeAdd)
        {
            var employee = new Employee
            {
                FirstName = employeeAdd.FirstName,
                LastName = employeeAdd.LastName,
                Email = employeeAdd.Email,
                AppliedPromocodesCount = 0

            };

            var id = await _employeeRepository.AddAsync(employee);

            return Ok(new { id = id });
        }

        /// <summary>
        /// Отредактировать сотрудника
        /// </summary>
        /// <returns></returns>

        [HttpPut]
        public async Task<IActionResult> EditAsync([FromBody] EditEmployeeRequest employeeEdit)
        {

            var chkEmployee = await _employeeRepository.GetByIdAsync(employeeEdit.Id);
             if (chkEmployee == null)
                return NotFound();

            var employee = new Employee
            {
                Id = employeeEdit.Id,
                FirstName = employeeEdit.FirstName,
                LastName = employeeEdit.LastName,
                Email = employeeEdit.Email,
                Roles = chkEmployee.Roles,
                AppliedPromocodesCount = chkEmployee.AppliedPromocodesCount
            };
            
            await _employeeRepository.EditAsync(employee);
            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> RemoveAsync(Guid id)
        {

            if (!await _employeeRepository.RemoveAsync(id)) 
                return NotFound();
            return Ok();
        }


    }
}